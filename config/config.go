package config

import (
	"mygo_projects/rest_empty/models"
)

func Load() models.Config {
	config := models.Config{}

	config.LogLevel = "debug"
	config.HTTPPort = ":8182"
	return config
}
