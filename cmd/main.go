package main

import (
	"mygo_projects/rest_empty/config"
	"mygo_projects/rest_empty/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()
	router := gin.Default()

	router.GET("/ping", getPing)
	router.POST("/post1", getPost1)
	router.Use(CORSMiddleware())
	router.Run(cfg.HTTPPort)
}

func getPing(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "pong",
	})
}

func getPost1(c *gin.Context) {
	var request models.Request
	err := c.ShouldBindJSON(&request)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, models.Response{
			ErrorCode: -1,
			ErrorMsg:  "wrong JSON fromat",
		})
		return
	}

	c.JSON(http.StatusCreated, models.Response{
		ErrorCode: 0,
		ErrorMsg:  "Success",
	})
	return
}

//CORSMiddleware ...
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
